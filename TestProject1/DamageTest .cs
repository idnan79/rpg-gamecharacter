﻿using Rpg_Characters.Attributes;
using Rpg_Characters.CustomeException;
using Rpg_Characters.Heroes;
using Rpg_Characters.Items;
using RpgCharacters.Items;
using System;
using Xunit;

namespace LevelTest
{
    public class DamageTest
    {
        [Fact]

        public void testabc()
        {

            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            Warrior war = new Warrior("Warrior");
            testAxe.ItemLevel = 2;
            // Act and Assert
            Assert.Throws<InvalidWeaponException>(() => war.Equip(testAxe));


        }


        [Fact]
        public void Armer()
        {

            Armor testPlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = Slot.SLOT_BODY,
                ArmorType = ArmorType.ARMOUR_PLATE,
                Attributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };
            Warrior war = new Warrior("Warrior");
            testPlateBody.ItemLevel = 1;
            Assert.Equal("New armor equipped!", war.Equip(testPlateBody));
        }
        /// <summary>
        /// testBow
        /// </summary>
        [Fact]
        public void testBow()
        {
            Weapon testBow = new Weapon()
            {
                ItemName = "Common bow",
                ItemLevel = 1,
                ItemSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponType.WEAPON_BOW,
                WeaponAttributes = new WeaponAttributes() { Damage = 12, AttackSpeed = 0.8 }
            };
        
            Warrior war = new Warrior("Warrior");
                testBow.ItemLevel = 1;
            Assert.Equal("New weapon equipped!", war.Equip(testBow));
        }
        /// <summary>
        /// testClolth
        /// </summary>
        [Fact]
        public void testClothHead()
        {
            Armor testClothHead = new Armor()
            {
                ItemName = "Common cloth head armor",
                ItemLevel = 1,
                ItemSlot = Slot.SLOT_HEAD,
                ArmorType = ArmorType.ARMOUR_CLOTH,
                Attributes = new PrimaryAttributes() { Vitality = 1, Intelligence = 5 }
            };
            Warrior war = new Warrior("Warrior");
            testClothHead.ItemLevel = 1 ;
            Assert.Equal("New armor equipped!", war.Equip(testClothHead));

        }



    }
}

