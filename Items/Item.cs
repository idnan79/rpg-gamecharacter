﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rpg_Characters.Items
{

    //All Avilable Slots
    public enum Slot
    {
        SLOT_WEAPON,
        SLOT_BODY,
        SLOT_LEGS,
        SLOT_HEAD
    }

    //Base value of Slots

    public abstract class Item
    {
        public string ItemName { get; set; }
        public int ItemLevel { get; set; }
        public Slot ItemSlot { get; set; }

        public abstract string ItemDescription();
    }

}
