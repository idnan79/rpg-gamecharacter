﻿using Rpg_Characters.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rpg_Characters.Items
{
    //List of Armor
    public enum ArmorType
    {
        ARMOUR_CLOTH,
        ARMOUR_LEATHER,
        ARMOUR_MAIL,
        ARMOUR_PLATE
    }

    public class Armor : Item
    {
        public ArmorType ArmorType { get; set; }
        public PrimaryAttributes Attributes { get; set; }


        public override string ItemDescription()
        {
            return $"Armor of type {ArmorType}";
        }

    }
}
