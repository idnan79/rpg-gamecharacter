﻿using Rpg_Characters.Attributes;
using Rpg_Characters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RpgCharacters.Items
{
    public enum WeaponType
    {
        WEAPON_AXE,
        WEAPON_BOW,
        WEAPON_DAGGER,
        WEAPON_HAMMER,
        WEAPON_STAFF,
        WEAPON_SWORD,
        WEAPON_WAND
    }

    
    /// Defines structure of an item of type weapon.
    
    public class Weapon : Item
    {
        public WeaponType WeaponType { get; set; }
        public WeaponAttributes WeaponAttributes { get; set; }

        /// <inheritdoc/>
        public override string ItemDescription()
        {
            return $"Weapon of type {WeaponType}";
        }


    }
}
