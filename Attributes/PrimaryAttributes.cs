﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rpg_Characters.Attributes
{
   public class PrimaryAttributes
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public int Vitality { get; set; }

        //Adding two primaryAttributes

        public static PrimaryAttributes operator +(PrimaryAttributes a, PrimaryAttributes b)
        {
            PrimaryAttributes total = new PrimaryAttributes();

            total.Strength = a.Strength + b.Strength;
            total.Dexterity = a.Dexterity + b.Dexterity;
            total.Intelligence = a.Intelligence + b.Intelligence;
            total.Vitality = a.Vitality + b.Vitality;

            return total;

        }
        //Checking if two primaryattributes are equal

        public override bool Equals(Object obj)
        {
            return obj is PrimaryAttributes attributes &&
                Strength == attributes.Strength &&
                Dexterity == attributes.Dexterity &&
                Intelligence == attributes.Intelligence &&
                Vitality == attributes.Vitality;
        }

    }
}
