﻿using Rpg_Characters.Attributes;
using Rpg_Characters.Items;
using RpgCharacters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rpg_Characters.Heroes
{
    public abstract class Hero
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public PrimaryAttributes BasePrimaryAttributes { get; set; }
        public PrimaryAttributes TotalPrimaryAttributes { get; set; }
        public SecondaryAttributes BaseSecondaryAttributes { get; set; }
        public Dictionary<Slot, Item> Equipment { get; set; }
        public double DPS { get; set; }

        
        // Initializes a hero.
        
        public Hero(string name, int strength, int dexterity, int intelligence, int vitality)
        {
            Name = name;
            Level = 1;
            Equipment = new Dictionary<Slot, Item>();
            BasePrimaryAttributes = new PrimaryAttributes() { Strength = strength, Dexterity = dexterity, Intelligence = intelligence, Vitality = vitality };
            CalculateTotalStats();
        }

       
        // Levels up a hero and recalculates total stats.
       
        public abstract void LevelUp(int levels);

       
        // Calculates a heroes damage per seconde.
       
        public abstract double CalculateDPS();

           // Equips a weapon.
       
        public abstract string Equip(Weapon weapon);

        
        // Equips armor.
        
        public abstract string Equip(Armor armor);


        
        // Calculates total stats based on base stats and equipped items.
        
        public void CalculateTotalStats()
        {
            TotalPrimaryAttributes = CalculateArmorBonus();
            BaseSecondaryAttributes = CalculateSecondaryStats();
            DPS = CalculateDPS();
        }

        
        // Calculates armor bonus
      
        public PrimaryAttributes CalculateArmorBonus()
        {
            PrimaryAttributes armorBonusValues = new() { Strength = 0, Dexterity = 0, Intelligence = 0, Vitality = 0 };

            bool hasHeadArmor = Equipment.TryGetValue(Slot.SLOT_HEAD, out Item headArmor);
            bool hasBodyArmor = Equipment.TryGetValue(Slot.SLOT_BODY, out Item bodyArmor);
            bool hasLegsArmor = Equipment.TryGetValue(Slot.SLOT_LEGS, out Item legsArmor);

            if (hasHeadArmor)
            {
                Armor a = (Armor)headArmor;
                armorBonusValues += new PrimaryAttributes() { Strength = a.Attributes.Strength, Dexterity = a.Attributes.Dexterity, Intelligence = a.Attributes.Intelligence, Vitality = a.Attributes.Vitality };
            }

            if (hasBodyArmor)
            {
                Armor a = (Armor)bodyArmor;
                armorBonusValues += new PrimaryAttributes() { Strength = a.Attributes.Strength, Dexterity = a.Attributes.Dexterity, Intelligence = a.Attributes.Intelligence, Vitality = a.Attributes.Vitality };
            }

            if (hasLegsArmor)
            {
                Armor a = (Armor)legsArmor;
                armorBonusValues += new PrimaryAttributes() { Strength = a.Attributes.Strength, Dexterity = a.Attributes.Dexterity, Intelligence = a.Attributes.Intelligence, Vitality = a.Attributes.Vitality };
            }

            return BasePrimaryAttributes + armorBonusValues;
        }

        public SecondaryAttributes CalculateSecondaryStats()
        {
            return new SecondaryAttributes()
            {
                Health = TotalPrimaryAttributes.Vitality * 10,
                ArmorRating = TotalPrimaryAttributes.Strength + TotalPrimaryAttributes.Dexterity,
                ElementalResistance = TotalPrimaryAttributes.Intelligence
            };
        }

        
        // Calculates a weapons damage per seconde.
       
        public double CalculateWeaponDPS()
        {
            Item equippedWeapon;
            bool hasWeapon = Equipment.TryGetValue(Slot.SLOT_WEAPON, out equippedWeapon);
            if (hasWeapon)
            {
                Weapon w = (Weapon)equippedWeapon;
                return w.WeaponAttributes.AttackSpeed * w.WeaponAttributes.Damage;
            }
            else
            {
                return 1;
            }
        }
    }
}

